document.getElementById("loadImage").addEventListener("click", loadImage);
document.getElementById("rocketLink").addEventListener("click", openLink);

function loadImage() {
  const apiKey = document.getElementById("apiKey").value;
  if (!apiKey) {
    alert("Please enter the API key");
    return;
  }
  // The NASA APOD service returns a JSON containing a URL to the picture
  const apiUrl = `https://api.nasa.gov/planetary/apod?api_key=${apiKey}`;
  fetch(apiUrl)
    .then((response) => response.json())
    .then((data) => {
      const imageUrl = data.url;
      document.getElementById(
        "imageContainer",
      ).innerHTML = `<img src="${imageUrl}" class="max-w-full h-auto" />`;
    })
    .catch((error) => console.error("Error:", error));
}

function openLink(event) {
  event.preventDefault();
  window.open("https://api.nasa.gov/", "_blank");
}
